const multer = require('multer');
const sharp = require('sharp');

const User = require('../../models/user_model');
const catchAsync = require('../../utils/catch_async');
const AppError = require('../../utils/app_error');
const factory = require('./handler_factory');


const filterBodyFields = (body, ...fields) => {
  const filteredBody = {};
  Object.keys(body).forEach(el => {
    if (fields.includes(el)) {
      filteredBody[el] = body[el];
    }
  });
  return filteredBody;
};

const upload = multer({
  storage: multer.memoryStorage(),
  fileFilter: (req, file, cb) => {
    if (file.mimetype.startsWith('image')) {
      return cb(null, true);
    }
    cb(new AppError('File is not image', 400), false);
  }
});

exports.resizeUserPhoto = catchAsync(async (req, res, next) => {
  if (!req.file) return next();

  // User can upload lots of images
  req.file.filename = `user-${req.user._id}-${Date.now()}.jpeg`;

  await sharp(req.file.buffer)
    .resize(500, 500)
    .toFormat('jpeg')
    .jpeg({
      quality: 90
    })
    .toFile(`public/img/users/${req.file.filename}`);
  next();
});

exports.uploadUserPhoto = upload.single('photo');

exports.getMe = (req, res, next) => {
  req.params.id = req.user._id;
  next();
};

exports.updateMyData = catchAsync(async (req, res, next) => {
  if (req.body.password || req.body.passwordConfirm) {
    return next(new AppError('This route is not for password update. Please use /update-password', 400));
  }

  const filteredBodyFields = filterBodyFields(req.body, 'name', 'email');
  if (req.file) filteredBodyFields.photo = req.file.filename;
  const user = await User.findByIdAndUpdate(req.user._id, filteredBodyFields, {
    new: true,
    runValidators: true
  });

  res.status(200).json({
    status: 'success',
    data: {
      user
    }
  });
});

exports.deleteMe = catchAsync(async (req, res) => {
  const user = await User.findByIdAndUpdate(req.user._id, {
    active: false
  });

  res.status(204).json({
    status: 'success',
    data: null
  });
});

exports.createUser = (req, res) => {
  res.status(500).json({
    status: 'error',
    message: 'This route is not defined! Please use /signup instead'
  });
};

exports.getUser = factory.getOne(User);
exports.getAllUsers = factory.getAll(User);
exports.updateUser = factory.updateOne(User);
exports.deleteUser = factory.deleteOne(User);
