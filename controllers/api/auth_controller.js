const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const crypto = require('crypto');

const User = require('../../models/user_model');
const catchAsync = require('../../utils/catch_async');
const AppError = require('../../utils/app_error');
const Email = require('../../utils/email');

const getSignToken = id => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  })
};

const createSendToken = (user, statusCode, req, res) => {
  const token = getSignToken(user._id);
  const cookieOptions = {
    expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
    httpOnly: true,
    secure: req.secure || req.headers['x-forwarded-proto'] === 'https'
  };

  user.password = undefined;
  res.cookie('jwt', token, cookieOptions);
  res.status(statusCode).json({
    status: 'success',
    token
  });
};

exports.signup = catchAsync(async(req, res) => {
  const url = `${req.protocol}://${req.get('host')}/me`;
  const user = await User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm
  });
  await new Email(user, url).sendWelcome();
  createSendToken(user, 201, req, res);
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new AppError('Provide email and password'), 400);
  }

  const user = await User.findOne({ email }).select('+password');

  if (!user || !(await user.isCorrectPasswords(password, user.password))) {
    return next(new AppError('Incorrect email or password', 401));
  }
  createSendToken(user, 200, req, res);
});

exports.logout = catchAsync((req, res, next) => {
  const cookieOptions = {
    expires: new Date(Date.now() - 1),
    httpOnly: true
  };

  if (process.env.NODE_ENV === 'production') {
    cookieOptions.secure = true;
  }

  res.cookie('jwt', '', cookieOptions);
  res.status(200).json({
    status: 'success'
  });
});

exports.protect = catchAsync(async (req, res, next) => {
  let token;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
    token = req.headers.authorization.split(' ')[1];
  } else if (req.cookies.jwt) {
    token = req.cookies.jwt;
  }

  if (!token) {
    return next(new AppError('You are not logged in! Pleace log in to get access.', 401));
  }

  //verification token
  const decodedToken = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

  //check if user still exists
  const user = await User.findById(decodedToken.id);
  if (!user) {
    return next(new AppError('The token belonging to this user does no longer exist', 401));
  }

  // check if user changed password after the token was issued
  if (user.passwordChangedAfterGetTokenCheck(decodedToken.iat)) {
    return next(new AppError('User recently changed password. Please log in again.', 401));
  }

  req.user = user;
  next();
});

exports.isLoggedIn = catchAsync(async (req, res, next) => {
  if (req.cookies.jwt) {
    const decodedToken = await promisify(jwt.verify)(req.cookies.jwt, process.env.JWT_SECRET);

    //check if user still exists
    const user = await User.findById(decodedToken.id);
    if (!user) {
      return next();
    }

    // check if user changed password after the token was issued
    if (user.passwordChangedAfterGetTokenCheck(decodedToken.iat)) {
      return next();
    }

    res.locals.user = user;
    return next();
  }
  next();
});

exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(new AppError('You does not have permission for this action', 403));
    }
    next();
  }
};

exports.forgotPassword = catchAsync(async (req, res, next) => {
  const user = await User.findOne({
    email: req.body.email
  });
  
  if (!user) {
    return next(new AppError('There is no user with email.', 404));
  }

  const resetToken = user.createPasswordResetToken();
  await user.save({ validateBeforeSave: false });

  const resetUrl = `${req.protocol}://${req.get('host')}/api/v1/users/reset-password/${resetToken}`;

  try {
    await new Email(user, resetUrl).sendPasswordReset();

    res.status(200).json({
      status: 'success',
      message: 'Token send to email'
    });
  } catch(e) {
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;

    await user.save({ validateBeforeSave: false });

    return next(new AppError('There was an error sending the email. Try again later', 500));
  }
});

exports.resetPassword = catchAsync(async (req, res, next) => {
  const resetPasswordToken = crypto.createHash('sha256').update(req.params.token).digest('hex');
  const user = await User.findOne({
    passwordResetToken: resetPasswordToken,
    passwordResetExpires: { $gt: Date.now() }
  });

  if (!user) {
    return next(new AppError('Token is not valid or expired', 400));
  }

  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  user.passwordResetExpires = undefined;
  user.passwordResetToken = undefined;
  await user.save();

  createSendToken(user, 200, req, res);
});

exports.updatePassword = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.user._id).select('+password');

  if (!user || !(await user.isCorrectPasswords(req.body.currentPassword, user.password))) {
    return next(new AppError('Your current password is wrong', 401));
  }
  
  user.password = req.body.newPassword;
  user.passwordConfirm = req.body.confirmNewPassword;
  await user.save();
  
  createSendToken(user, 200, req, res);
});
