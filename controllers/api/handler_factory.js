const catchAsync = require('../../utils/catch_async');
const AppError = require('../../utils/app_error');
const ApiFeatures = require('../../utils/api_features');

exports.getOne = (Model, populateParam) => catchAsync(async (req, res, next) => {
  let query = Model.findById(req.params.id);
  if (populateParam) query.populate(populateParam);
  const doc = await query;

  if (!doc) {
    return next(new AppError('Not found id', 404));
  }

  res.status(200).json({
    status: 'success',
    data: {
      data: doc
    }
  });
});

exports.getAll = Model => catchAsync(async (req, res) => {
  // Hack for merge tourId
  let filter = {};
  if (req.params.tourId) filter = { tour: req.params.tourId };

  const apiFeatures = new ApiFeatures(Model.find(), req.query)
    .filter(['page', 'sort', 'limit', 'fields'])
    .sort()
    .limit()
    .pagination();

  const doc = await apiFeatures.query;

  res.status(200).json({
    status: 'success',
    result: doc.length,
    data: {
      data: doc
    }
  });
});

exports.createOne = Model => catchAsync(async (req, res, next) => {
  const doc = await Model.create(req.body);
  res.status(201).json({
    status: 'success',
    data: {
      data: doc
    }
  });
});

exports.updateOne = Model => catchAsync(async (req, res, next) => {
  const doc = await Model.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!doc) {
    return next(new AppError('Not found id', 404));
  }

  res.status(200).json({
    status: 'success',
    data: {
      data: doc
    }
  });
});

exports.deleteOne = Model => catchAsync(async (req, res, next) => {
  const doc = await Model.findByIdAndDelete(req.params.id);

  if (!doc) {
    return next(new AppError('Not found id', 404));
  }

  res.status(204).json({
    status: 'success',
    data: null
  });
});
