const path = require('path');
const express = require('express');
const morgan = require('morgan');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const hpp = require('hpp');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const cors = require('cors');

const app = express();

app.enable('trust proxy');

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'public'), {
  //extensions: ['html', 'htm', 'map'] //example
}));

const limiter = rateLimit({
  max: process.env.RATE_LIMIT,
  windowMs: 60 * 60 * 1000,
  message: 'Too many requests from this IP, please try again in an hour'
});

const AppError = require('./utils/app_error');
const globalErrorHandler = require('./controllers/api/error_controller');
const tourRouter = require('./routes/api/tour_routes');
const userRouter = require('./routes/api/user_routes');
const reviewRouter = require('./routes/api/review_routes');
const viewRouter = require('./routes/view_routes');
const bookingRouter = require('./routes/api/booking_routes');
const bookingController = require('./controllers/api/booking_controller');

/**
 * Global middlewares
 */

// CORS
app.use(cors());
app.options('*', cors());

app.use(compression());

// Set security HTTP headers
app.use(helmet());

// Limit requests from same IP
app.use('/api', limiter);

// Stripe webhook, BEFORE body-parser, because stripe needs the body as stream
app.post(
  '/webhook-checkout',
  express.raw({ type: 'application/json' }),
  bookingController.webhookCheckout
);

// Development logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// POST body parser from req.body
app.use(express.json({
  limit: '10kb'
}));

// Cookie parser
app.use(cookieParser());

// Data sanitization aganist NoSQL query injection
app.use(mongoSanitize());

// Data sanitization aganist XSS
app.use(xss());

// Prevent parameter pollution
app.use(hpp({
    whitelist: [
      'duration',
      'ratingsQuantity',
      'ratingsAverage',
      'maxGroupSize',
      'difficulty',
      'price'
    ]
  }
));

/**
 * End global middlewares
 */
app.use('/', viewRouter);
app.use('/api/v1/tours', tourRouter);
app.use('/api/v1/users', userRouter);
app.use('/api/v1/review', reviewRouter);
app.use('/api/v1/bookings', bookingRouter);

app.all('*', (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`));
});

app.use(globalErrorHandler);

module.exports = app;
