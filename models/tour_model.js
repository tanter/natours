const mongoose = require('mongoose');
const slugify = require('slugify');
const validator = require('validator');

const tourSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'A tour mast have a name'],
    unique: true,
    trim: true,
    maxlength: [40, 'A tour name must have less or equal then 40 characters'],
    minlength: [4, 'A tour name must have more or equal then 4 characters']
    // validate: [validator.isAlpha, 'Tour name must only contain characters']
  },
  slug: {
    type: String
  },
  price: {
    type: Number,
    required: [true, 'A tour mast have a price']
  },
  priceDiscount: {
    type: Number,
    validate: {
      validator: function(value) {
        return value < this.price;
      },
      message: 'Discount price ({VALUE}) should be below regular price'
    }
  },
  duration: {
    type: Number,
    required: [true, 'A tour mast have a duration']
  },
  maxGroupSize: {
    type: Number,
    required: [true, 'A tour mast have a maxGroupSize']
  },
  difficulty: {
    type: String,
    required: [true, 'A tour mast have a difficulty'],
    enum: {
      values: ['easy', 'medium', 'difficult'],
      message: 'Difficulty is either: easy, medium, difficult'
    }
  },
  ratingsAverage: {
    type: Number,
    default: 4.5,
    min: [1, 'Rating must be above 1.0'],
    max: [5, 'Rating must be below 5.0'],
    set: val => Math.round(val * 10) / 10
  },
  ratingsQuantity: {
    type: Number,
    default: 0
  },
  summary: {
    type: String,
    trim: true,
    required: [true, 'A tour mast have a summary']
  },
  description: {
    type: String,
    trim: true
  },
  imageCover: {
    type: String,
    required: [true, 'A tour mast have a imageCover']
  },
  images: {
    type: [String]
  },
  createAt: {
    type: Date,
    default: Date.now(),
    select: false
  },
  startDates: {
    type: [Date]
  },
  secretTour: {
    type: Boolean,
    default: false
  },
  startLocation: {
    type: {
      type: String,
      default: 'Point',
      enum: ['Point'],
    },
    coordinates: [Number],
    description: String,
    address: String
  },
  locations: [
    {
      type: {
        type: String,
        default: 'Point',
        enum: ['Point'],
      },
      coordinates: [Number],
      address: String,
      description: String,
      day: Number
    }
  ],
  guides: [
    {
      type: mongoose.Schema.ObjectId,
      ref: 'User'
    }
  ]
},
{
  toJSON: { virtuals: true },
  toObject: { virtuals: true }
});

tourSchema.index({ price: 1, ratingsAverage: -1 });
tourSchema.index({ slug: 1 });
tourSchema.index({ startLocation: '2dsphere' });

tourSchema.virtual('durationWeeks').get(function() {
  return this.duration / 7;
});

tourSchema.virtual('reviews', {
  ref: 'Review',
  localField: '_id',
  foreignField: 'tour'
});

// Document middleware
tourSchema.pre('save', function(next) {
  this.slug = slugify(this.name, {
    replacement: '_',
    lower: true
  });
  next();
});

tourSchema.post('save', function(doc, next) {
  console.log('Document have been created or save');
  next();
});

// Query middleware
tourSchema.pre(/^find/, function(next) {
  this.find({ secretTour: { $ne: true } });
  this.start = Date.now();
  next();
});

tourSchema.pre(/^find/, function(next) {
  this.populate({
    path: 'guides',
    select: '-__v -passwordChangedAt'
  });
  next();
})

tourSchema.post(/^find/, function(docs, next) {
  console.log(`Query took - ${Date.now() - this.start} milliseconds...`);
  next();
});

// Aggregation meddleware
tourSchema.pre('aggregate', function(next) {
  this.pipeline().push({
    $match: {
      secretTour: {
        $ne: true
      }
    }
  });
  next();
});

const Tour = mongoose.model('Tour', tourSchema);

module.exports = Tour;
