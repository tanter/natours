import '@babel/polyfill';

import { login, logout } from './login';
import { signup } from './register';
import { updateSettings, updatePassword } from './update_settings';
import { drowMapBox } from './mapbox';
import { bookTour } from './stripe';
import { showAlert } from './alerts';

window.addEventListener('load', () => {
  if (document.body.dataset.alert) {
    showAlert('success', document.body.dataset.alert, 10);
  }

  if (document.getElementById('login_form')) {
    document.getElementById('login_form').addEventListener('submit', e => {
      e.preventDefault();
      const email = document.getElementById('email').value;
      const password = document.getElementById('password').value;
      login(email, password);
    });
  }

  if (document.getElementById('signup_form')) {
    document.getElementById('signup_form').addEventListener('submit', async e => {
      e.preventDefault();
      const btnSubmit = document.querySelector('#signup_form .btn--green');
      const btnSubmitText = btnSubmit.textContent;
      btnSubmit.textContent = 'Processing...';
      const name = document.getElementById('name').value;
      const email = document.getElementById('email').value;
      const password = document.getElementById('password').value;
      const passwordConfirm = document.getElementById('password_confirm').value;
      await signup(name, email, password, passwordConfirm);
      btnSubmit.textContent = btnSubmitText;
    });
  }

  if (document.getElementById('logout')) {
    document.getElementById('logout').addEventListener('click', () => logout());
  }

  if (document.getElementById('update_settings_form')) {
    document.getElementById('update_settings_form').addEventListener('submit', async e => {
      e.preventDefault();
      const btnSubmit = document.querySelector('#update_settings_form .btn--green');
      const btnSubmitText = btnSubmit.textContent;
      btnSubmit.textContent = 'Updating...';
      const form = new FormData();
      form.append('name', document.getElementById('name').value);
      form.append('email', document.getElementById('email').value);
      form.append('photo', document.getElementById('photo').files[0]);
      const res = await updateSettings(form);
      const photo = `/img/users/${res.data.data.user.photo}`;
      document.querySelector('.form__user-photo').src = photo;
      document.querySelector('.nav__user-img').src = photo;
      btnSubmit.textContent = btnSubmitText;
    });
  }

  if (document.getElementById('change_password_form')) {
    document.getElementById('change_password_form').addEventListener('submit', async e => {
      e.preventDefault();
      const btnSubmit = document.querySelector('#change_password_form .btn--green');
      const btnSubmitText = btnSubmit.textContent;
      btnSubmit.textContent = 'Updating...';
      const currentPassword = document.getElementById('password_current').value;
      const newPassword = document.getElementById('password').value;
      const confirmNewPassword = document.getElementById('password_confirm').value;
      await updatePassword(currentPassword, newPassword, confirmNewPassword);
      document.getElementById('change_password_form').reset();
      btnSubmit.textContent = btnSubmitText;
    });
  }

  if (document.getElementById('book_tour')) {
    document.getElementById('book_tour').addEventListener('click', e => {
      e.target.textContent = 'Processing...';
      const { tourId } = e.target.dataset;
      bookTour(tourId);
      e.target.textContent = 'Processing...'
    });
  }

  if (document.getElementById('map')) drowMapBox();
});
