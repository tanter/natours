import axios from 'axios';
import { showAlert } from './alerts';

export const signup = async (name, email, password, passwordConfirm) => {
  try {
    const res = await axios({
      method: 'POST',
      url: '/api/v1/users/signup',
      data: {
        name,
        email,
        password,
        passwordConfirm
      }
    });

    if (res.status === 201) {
      showAlert('success', 'Registration successfully');
      setTimeout(() => {
        window.location.assign('/me');
      }, 400);
    }
  } catch (e) {
    showAlert('error', e.response.data.message);
  }
};
