import axios from 'axios';
import { showAlert } from './alerts';

export const login = async (email, password) => {
  try {
    const res = await axios({
      method: 'POST',
      url: '/api/v1/users/login',
      data: {
        email,
        password
      }
    });

    if (res.status === 200) {
      showAlert('success', 'Logged in successfully');
      setTimeout(() => {
        window.location.assign('/');
      }, 400);
    }
  } catch (e) {
    showAlert('error', e.response.data.message);
  }
};

export const logout = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: '/api/v1/users/logout'
    });
    if (res.status === 200) location.reload(true);
  } catch (e) {
    showAlert('error', 'Error logout. Try again');
  }
};
