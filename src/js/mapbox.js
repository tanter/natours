export function drowMapBox() {
  const locations = JSON.parse(document.getElementById('map').dataset.locations);

  mapboxgl.accessToken = 'pk.eyJ1IjoidGFudGVyNzc3IiwiYSI6ImNrMzJ2dDJlcjBtNW4zbXMzc3hmczZnMGgifQ.ZoGcmwYwwU6Q_v2h_1fXjQ';

  const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/tanter777/ck32vu18l35mv1co8ztnv54g2',
    scrollZoom: false
  });

  const bounds = new mapboxgl.LngLatBounds();

  locations.forEach(loc => {
    // Create marker
    const el = document.createElement('div');
    el.className = 'marker';

    // Add marker
    new mapboxgl.Marker({
      element: el,
      anchor: 'bottom'
    })
      .setLngLat(loc.coordinates)
      .addTo(map);

    // Add popup
    new mapboxgl.Popup({
      offset: 30
    })
      .setLngLat(loc.coordinates)
      .setHTML(`<p>Day ${loc.day}: ${loc.description}</p>`)
      .addTo(map);

    // Extend map bounds to include current location
    bounds.extend(loc.coordinates);
  });

  map.fitBounds(bounds, {
    padding: {
      top: 250,
      bottom: 200,
      left: 150,
      right: 150
    }
  });
}
