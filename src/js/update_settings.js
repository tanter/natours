import axios from 'axios';
import { showAlert } from './alerts';

export const updateSettings = async (data) => {
    try {
      const res = await axios({
        method: 'PATCH',
        url: '/api/v1/users/update-my-data',
        data
      });

      if (res.status === 200) showAlert('success', 'Your settings have been updated');
      return res;
    } catch (e) {
      showAlert('error', e.response.data.message);
    }
};

export const updatePassword = async (currentPassword, newPassword, confirmNewPassword) => {
  try {
    const res = await axios({
      method: 'PATCH',
      url: '/api/v1/users/update-password',
      data: {
        currentPassword,
        newPassword,
        confirmNewPassword
      }
    });

    if (res.status === 200) showAlert('success', 'Your password have been updated');
  } catch (e) {
    showAlert('error', e.response.data.message);
  }
};
