const { Router } = require('express');

const viewController = require('../controllers/view_controller');
const authController = require('../controllers/api/auth_controller');
const bookingController = require('../controllers/api/booking_controller');

const router = Router();

router.use(viewController.alerts);

router.get('/',
  authController.isLoggedIn,
  viewController.getOverview
);

router.get('/tour/:slug',
  authController.isLoggedIn,
  viewController.getTour
);

router.get('/login',
  authController.isLoggedIn,
  viewController.login
);

router.get('/signup',
  viewController.signup
);

router.get('/me',
  authController.isLoggedIn,
  authController.protect,
  viewController.getAccount
);

router.get('/my-tours',
  authController.isLoggedIn,
  authController.protect,
  viewController.getMyTours
);

module.exports = router;
