const { Router } = require('express');

const userController = require('../../controllers/api/user_controller');
const authController = require('../../controllers/api/auth_controller');

const router = Router();

router.post('/signup', authController.signup);
router.post('/login', authController.login);
router.get('/logout', authController.logout);
router.post('/forgot-password', authController.forgotPassword);
router.patch('/reset-password/:token', authController.resetPassword);

router.patch('/update-password',
  authController.protect,
  authController.updatePassword
);

// Protect all routes after this middleware
router.use(authController.protect);

router.get('/me',
  authController.protect,
  userController.getMe,
  userController.getUser
);

router.patch('/update-my-data',
  authController.protect,
  userController.uploadUserPhoto,
  userController.resizeUserPhoto,
  userController.updateMyData
);

router.delete('/delete-me',
  authController.protect,
  userController.deleteMe
);

router.use(authController.restrictTo('admin'));

router
  .route('/')
  .get(userController.getAllUsers)
  .post(userController.createUser);

router
  .route('/:id')
  .get(userController.getUser)
  .patch(userController.updateUser)
  .delete(userController.deleteUser);

module.exports = router;
